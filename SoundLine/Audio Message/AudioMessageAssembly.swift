//
//  AudioMessageAssembly.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import Foundation

final class AudioMessageAssembly {
    func module(with audioMessage: AudioMessage) -> AudioMessageView {
        let viewModel = AudioMessageViewModel(with: audioMessage)
        return AudioMessageView(viewModel: viewModel)
    }
}
