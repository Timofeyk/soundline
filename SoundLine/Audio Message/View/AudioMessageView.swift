
//
//  LineView.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import UIKit

final class AudioMessageView: UIView {
    // MARK: - Constants
    private let buttonSide = CGFloat(30)
    private let inset = CGFloat(10)
    private let countdownWidth = CGFloat(50)
    
    // MARK: - Dependencies
    private let viewModel: AudioMessageViewModel
    private var displayLink: CADisplayLink? = nil
    
    // MARK: - Subviews
    private let playbackButton = UIButton()
    private let timelineView: TimelineProgressView
    private let countdownLabel = UILabel()
    
    // MARK: - Init
    init(viewModel: AudioMessageViewModel) {
        self.viewModel = viewModel
        self.timelineView = TimelineProgressView(timeline: viewModel.timeline)
        super.init(frame: .zero)
        
        displayLink = CADisplayLink(target: self, selector: #selector(onDisplayLinkUpdate))
        displayLink?.add(to: .current, forMode: .common)
        setupSubviews()
        setupCallbacks()
        updateCountdownLabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        playbackButton.frame = CGRect(x: 0,
                                      y: 0,
                                      width: bounds.height,
                                      height: bounds.height)
        
        countdownLabel.frame = CGRect(x: bounds.width - countdownWidth,
                                      y: 0,
                                      width: countdownWidth,
                                      height: bounds.height)
        
        let timelineX = playbackButton.frame.maxX + inset
        timelineView.frame = CGRect(x: timelineX,
                                    y: 0,
                                    width: countdownLabel.frame.minX - timelineX - inset,
                                    height: bounds.height)
        
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let timelineSize = timelineView.sizeThatFits(CGSize(width: size.width, height: buttonSide))
        return CGSize(width: buttonSide + timelineSize.width + 2 * inset + countdownWidth,
                      height: buttonSide)
    }
    
    // MARK: - Actions
    @objc private func onPlaybackButtonTap() {
        viewModel.togglePlaybackState()
    }
    
    // MARK: - Setup
    private func setupSubviews() {
        playbackButton.tintColor = .black
        countdownLabel.textColor = .black
        playbackButton.addTarget(self, action: #selector(onPlaybackButtonTap), for: .touchUpInside)
        addSubview(playbackButton)
        addSubview(timelineView)
        addSubview(countdownLabel)
    }
    
    private func setupCallbacks() {
        viewModel.onPlaybackStateUpdate = { [weak self] in
            self?.updatePlaybackState()
        }
    }
    
    // MARK: - CADisplayLink action
    @objc private func onDisplayLinkUpdate() {
        timelineView.progress = viewModel.progress
        updateCountdownLabel()
    }
    
    // MARK: - Private
    private func updateCountdownLabel() {
        countdownLabel.text = viewModel.coundownValue
    }
    
    // MARK: - Player appearance
    private func updatePlaybackState() {
        switch viewModel.playbackState {
        case .initial:
            displayLink?.isPaused = true
            onDisplayLinkUpdate()
            playbackButton.setImage(UIImage(named: "ic_play"), for: .normal)
        case .paused:
            displayLink?.isPaused = true
            playbackButton.setImage(UIImage(named: "ic_play"), for: .normal)
        case .waitingForPause:
            displayLink?.isPaused = true
            playbackButton.setImage(UIImage(named: "ic_play"), for: .normal)
        case .playing:
            displayLink?.isPaused = false
            playbackButton.setImage(UIImage(named: "ic_pause"), for: .normal)
        case .waitingForPlay:
            displayLink?.isPaused = true
            playbackButton.setImage(UIImage(named: "ic_pause"), for: .normal)
        }
    }
}
