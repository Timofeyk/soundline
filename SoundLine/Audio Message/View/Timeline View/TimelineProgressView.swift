//
//  TimelineProgressView.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import UIKit

final class TimelineProgressView: UIView {
    // MARK: - Subviews | Sublayers
    private let progressLayer = CALayer()
    private let timelineView: TimelineView
    
    // MARK: - Variables
    var progress: Float = 0 {
        didSet {
            progressLayer.frame = CGRect(x: 0,
                                         y: 0,
                                         width: bounds.width * CGFloat(progress),
                                         height: bounds.height)
        }
    }
    
    // MARK: - Init
    init(timeline: [Float]) {
        self.timelineView = TimelineView(timeline: timeline)
        super.init(frame: .zero)
        isOpaque = false
        backgroundColor = UIColor(white: 0, alpha: 0.5)
        progressLayer.backgroundColor = UIColor.black.cgColor
        layer.addSublayer(progressLayer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        
        var progressLayerFrame = progressLayer.frame
        progressLayerFrame.size.height = bounds.height
        progressLayer.frame = progressLayerFrame
        
        timelineView.frame = bounds
        layer.mask = timelineView.layer
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return timelineView.sizeThatFits(size)
    }
}
