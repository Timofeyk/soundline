//
//  TimelineView.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import UIKit

final class TimelineView: UIView {
    private let interitemSpacing = CGFloat(2)
    private let sampleWidth = CGFloat(4)
    let timeline: [Float]
    
    // MARK: - Init
    init(timeline: [Float]) {
        self.timeline = timeline
        super.init(frame: .zero)
        isOpaque = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Drawing
    override func draw(_ rect: CGRect) {
        guard !timeline.isEmpty else { return }
        let (sampleHeightMin, sampleHeightMax) = (CGFloat(6), CGFloat(28))
        var offset = CGFloat(0)
        
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(UIColor.black.cgColor)
        timeline.forEach { sample in
            let sampleHeight = (sampleHeightMax - sampleHeightMin) * CGFloat(sample) + sampleHeightMin
            let sampleRect = CGRect(x: offset,
                                          y: bounds.height - sampleHeight,
                                          width: sampleWidth,
                                          height: sampleHeight)
            let roundedRectPath = UIBezierPath(roundedRect: sampleRect, cornerRadius: sampleWidth / 2)
            context?.addPath(roundedRectPath.cgPath)
            context?.fillPath()
            
            offset = sampleRect.maxX + interitemSpacing
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: (sampleWidth + interitemSpacing) * CGFloat(timeline.count), height: size.height)
    }
}
