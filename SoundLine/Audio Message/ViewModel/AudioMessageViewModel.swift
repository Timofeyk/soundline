//
//  AudioMessageViewModel.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import Foundation

final class AudioMessageViewModel {
    
    // MARK: - View playback state
    enum PlaybackState {
        case initial
        case paused
        case playing
        case waitingForPlay
        case waitingForPause
        
        init(playerPlaybackState: Player.PlaybackState) {
            switch playerPlaybackState {
            case .play:
                self = .playing
            case .paused:
                self = .paused
            case .stop:
                self = .initial
            }
        }
        
        mutating func toggle() {
            switch self {
            case .initial, .paused, .waitingForPause:
                self = .waitingForPlay
            case .playing, .waitingForPlay:
                self = .waitingForPause
            }
        }
    }
    
    // MARK: - Dependencies
    private let decoder: AudioTimelineDecoder
    private let player: Player
    private let playerQueue: DispatchQueue
    
    // MARK: - Properties
    let duration: TimeInterval
    let timeline: [Float]
    var playbackState: PlaybackState {
        didSet {
            onPlaybackStateUpdate?()
        }
    }
    var coundownValue: String {
        let value = Int(player.duration - player.currentTime)
        let minutes = (value % 3600) / 60
        let seconds = (value % 3600) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    var progress: Float {
        player.progress
    }
    
    // MARK: - Callbacks
    var onPlaybackStateUpdate: (() -> Void)? = nil {
        didSet {
            onPlaybackStateUpdate?()
        }
    }
    
    // MARK: - Init
    init(with audioMessage: AudioMessage) {
        duration = audioMessage.duration
        playbackState = .paused
        player = Player(duration: audioMessage.duration)
        decoder = AudioTimelineDecoder(rawTimeline: audioMessage.rawTimeline)
        timeline = decoder.decode()
        playerQueue = DispatchQueue(label: "app.sound.line.player.queue")
        player.onStateUpdate = { [weak self] playbackState in
            DispatchQueue.main.async {
                self?.playbackState = .init(playerPlaybackState: playbackState)
            }
        }
    }
    
    // MARK: - Playback
    func togglePlaybackState() {
        playbackState.toggle()
        
        playerQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.player.state == .play {
                strongSelf.player.pause()
            } else {
                strongSelf.player.play()
            }
        }
    }
}
