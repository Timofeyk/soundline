//
//  AudioTimelineDecoder.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import Foundation
import AVFoundation

final class AudioTimelineDecoder {
    
    // MARK: - Constants
    private let sampleLimit = Float(80)
    
    // MARK: - Properties
    private let rawTimeline: [Float]
    init(rawTimeline: [Float]) {
        self.rawTimeline = rawTimeline
    }
    
    func decode() -> [Float] {
        /// Выбираем предельное для нас кромкость "сэмпла", всё что выше приравниваем к лимиту
        /// Решает 2 проблемы:
        ///  1. Убирает слишком высокие всплески, если аудиозаписть тихая, но есть пара очень громких моментов. Иначе таймлайн получится приравнен к низу
        ///  2. Оптимальнее, чем искать максимум и выравнивать всё по нему
        ///
        /// Этот декодер можно сделать асинхронным, как только вычисления станут критично тяжелыми. В этом случае таймлайн надо будет рисовать асинронно, через какое-то время после появления самого view на экране
        let normaized = rawTimeline.map { min($0, sampleLimit) / sampleLimit }
        
        /// Если число сэмплов не попадает по необходимый лимит (от 10 до 30), то отбираем последовательные таймлайны, прореживая или добавляя новые
        let samplesRange = 10...30
        if samplesRange.contains(normaized.count) {
            return normaized
        } else {
            let targetSamplesCount: Int
            if normaized.count > samplesRange.max() ?? 0 {
                targetSamplesCount = samplesRange.max() ?? 0
            } else {
                targetSamplesCount = samplesRange.min() ?? 0
            }
                
            var reduced: [Float] = []
            let multiplier = Float(normaized.count) / Float(targetSamplesCount)
            for index in 0..<targetSamplesCount {
                reduced.append(normaized[Int(Float(index) * multiplier)])
            }
            return reduced
        }
    }
}
