//
//  Player.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import Foundation


// Mock - класс, иммитирующий работу плеера
final class Player {
    
    enum PlaybackState {
        case stop
        case play
        case paused
    }
    
    // MARK: - Timing state
    let duration: TimeInterval
    var progress: Float {
        guard duration != 0 else { return 0 }
        return max(Float(0.0), min(Float(1.0), Float(currentTime / duration)))
    }
    private var cachedTime: TimeInterval = 0
    private var startTime = Date()
    var currentTime: TimeInterval {
        Date().timeIntervalSince(startTime)
    }
    
    // MARK: - Dependencies
    private var timer: Timer? = nil
    
    // MARK: - Properties
    var state: PlaybackState = .stop {
        didSet {
            onStateUpdate?(state)
        }
    }
    
    // MARK: - Callbacks
    var onStateUpdate: ((PlaybackState) -> Void)? = nil
    
    // MARK: - Init
    init(duration: TimeInterval) {
        self.duration = duration
    }

    // MARK: - Playback control
    func play() {
        sleep(UInt32.random(in: 0...1)) // Симулируем запуск плеера :)
        if state == .stop {
            startTime = Date()
        } else {
            startTime = Date().addingTimeInterval(-cachedTime)
            cachedTime = 0
        }
        startTimer()
        state = .play
    }

    func pause() {
        state = .paused
        cachedTime = currentTime
        pauseTimer()
    }
    
    func stop() {
        cachedTime = 0
        startTime = Date()
        state = .stop
        destroyTimer()
    }
    
    // MARK: - Timer
    private func startTimer() {
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(withTimeInterval: self.duration - self.currentTime, repeats: false, block: { [weak self] timer in
                self?.stop()
            })
        }
    }
    
    private func pauseTimer() {
        timer?.invalidate()
    }
    
    private func destroyTimer() {
        timer?.invalidate()
        timer = nil
    }
    
}
