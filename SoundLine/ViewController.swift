//
//  ViewController.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    private var audioMessageView: UIView?
    private var nextButton: UIButton?
    
    // MARK: - ViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nextButton = UIButton()
        nextButton.addTarget(self, action: #selector(onNextButtonTap), for: .touchUpInside)
        nextButton.setTitle("Next", for: .normal)
        nextButton.setTitleColor(.black, for: .normal)
        view.addSubview(nextButton)
        self.nextButton = nextButton
            
        generateAudioMessage()
    }
    
    // MARK: - Layout
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let messageViewSize = audioMessageView?.sizeThatFits(view.bounds.size) {
            audioMessageView?.frame = CGRect(x: view.bounds.width / 2 - messageViewSize.width / 2,
                                             y: view.bounds.height / 2 - messageViewSize.height / 2,
                                             width: messageViewSize.width,
                                             height: messageViewSize.height)
        }
        
        if let nextButtonSize = nextButton?.sizeThatFits(view.bounds.size) {
            nextButton?.frame = CGRect(x: view.bounds.width / 2 - nextButtonSize.width / 2,
                                       y: view.bounds.height / 2 - nextButtonSize.height / 2 + 200,
                                       width: nextButtonSize.width,
                                       height: nextButtonSize.height)
        }

    }
    
    // MARK: - Private
    private func generateAudioMessage() {
        audioMessageView?.removeFromSuperview()
        
        var timeline: [Float] = []
        for _ in 1...Int.random(in: 1...100) {
            timeline.append(Float.random(in: 0...100))
        }
        let duration = Double.random(in: 0...100)
        let audioMessage = AudioMessage(rawTimeline: timeline, duration: duration)
        let audioMessageView = AudioMessageAssembly().module(with: audioMessage)
        view.addSubview(audioMessageView)
        self.audioMessageView = audioMessageView
    }
    
    @objc private func onNextButtonTap() {
        generateAudioMessage()
    }

}

