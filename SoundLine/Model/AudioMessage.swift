//
//  AudioMessage.swift
//  SoundLine
//
//  Created by Timofey on 02/03/2020.
//  Copyright © 2020 app.sound.line. All rights reserved.
//

import Foundation

struct AudioMessage {
    let rawTimeline: [Float] // условный PCM буффер без даунсемплинга и выравнивания
    let duration: TimeInterval
}

